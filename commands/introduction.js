const { description, execute } = require("./clear");

module.exports = {
    name: 'introduction',
    description: 'KaitoKunnnnnnnn',
    execute(message) {
        embed.setColor("#0003b8")
        .setThumbnail("https://upload.wikimedia.org/wikipedia/commons/thumb/b/b1/Kaito_Kid_signature.svg/1200px-Kaito_Kid_signature.svg.png")
        .setTitle("Hello World!")
        .addField("🃏Kaito Kun", "KaitoKun is a Discord Bot")
        .addField("🃏Character", "◈Age: 17\n◈Gender: Male\n◈Height: 174 cm (5'8.5\")\n◈Weight: 58 kg (128 lbs)\n◈Date of birth: June 21")
        .addField("🃏Server", "If you know bug or glitchs,\nPlease let we known in\n<#719848475847819402> ↤\nOr you have any questions you can ask in\n<#719848475847819403> ↤")
        .addField("🃏Invite", "[[Click Here]](https://top.gg/bot/689711767827251215)")
        .addField("🃏Website", "[[Click Here]](https://kaitobot.tk/)")
        .addField("🃏Support Server", "[[Kaito Bot Support]](https://discord.io/KaitoSupport)")
        message.channel.send(embed)
    }
}