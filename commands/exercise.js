module.exports = {
    name: 'breathe',
    description: 'breathe exercise',
    execute(message) {
        let embed = new Discord.MessageEmbed()
        embed.setColor("BLUE")
        embed.setTitle("Breathing Exercises")
            embed.setImage('https://i.imgur.com/2nkt1PW.gif?noredirect')
            embed.setThumbnail('https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTp6Pjjkc9jCxme-OqYvO8qGM943omRK7QfH1KgHXAUya6uGZj9&usqp=CAU')
            embed.setDescription('Focusing on the breath is one of the most common and fundamental techniques for accessing the meditative state.\nBreath is a deep rhythm of the body that connects us intimately with the world around us.\n\nLearn More? [[Click here]](https://mumblesandthings.tumblr.com/post/144679902302/breathing-exercises)')
            embed.setFooter(message.author.username, message.author.displayAvatarURL);
        message.channel.send(embed);
    }
}