module.exports = {
    name: 'avatar',
    description: 'Your face',
    execute(message) {
        let avatarembed = new Discord.MessageEmbed()
        .setColor("RANDOM")
        .setTitle(`${message.author.username}'s Avatar`)
        .setImage(message.author.displayAvatarURL())
        .setFooter("This is you")
        message.channel.send(avatarembed)
    }
}