  module.exports = {
    name: 'help',
    aliases: ['all', 'cmd'],
	description: 'Show all message',
	execute(message) {
        const { MessageEmbed } = require('discord.js');
        const Discord = require('discord.js')
        const embed = new MessageEmbed()
        const client = new Discord.Client()
        .setTitle('Help list')
        // Set the color of the embed
        .setColor(`RANDOM`)
          .setTitle(`*${client.user.tag}'s Commands*`)
          .setThumbnail(client.user.avatarURL())
          .addField("🔰Introduction command", "`introduction`")
          .addField("🔨Moderation Commands", "`kick` `ban` ``")
          .addField("🖼Image Commands", "`meme` `gore` `conan` `puppy` `osu`")
          .addField("ℹInformation Commands", "`avatar` `serverinfo` `ping` `profile`")
          .addField("😜Fun Commands","`joke` `cnjoke` `dice` `8ball `dm`")
          .addField("😷TakeCare Commands", "`covid` `breathe`")
          .addField("💁‍♂️Creators Support Commands", "`donate` `website` `thaisite` `osume` `sub` `subr` `subg`")
          .addField("💿Misc Commands", "`owo` `hello` `d!` `dm`")
          .addField("🧠Config Commands", "`newCMD` `prefix`")
          .setTimestamp()
          .setFooter("All Commands")
      message.channel.send(embed);
	},
};