module.exports = {
    name: 'puppy',
    description: 'Puppy-fluffy',
	execute: async (message) => {
        const subReddits = ["puppy"]
        const random = subReddits[Math.floor(Math.random() * subReddits.length)];
  
        const img = await randomPuppy(random);
        const embed = new Discord.MessageEmbed()
        .setColor("RANDOM")
        .setImage(img)
        .setTitle(`From r/${random}`)
        .setURL(`https://reddit.com/r/${random}`)
        .setDescription(`Animal and stuff`);
        message.channel.send(embed)
    }
}