module.exports = {
	name: 'conan',
	description: 'Just CONAN',
	execute: async (message) => {
        const subReddits = ["OneTruthPrevails"]
        const random = subReddits[Math.floor(Math.random() * subReddits.length)];
  
        const img = await randomPuppy(random);
        const embed = new Discord.MessageEmbed()
        .setColor("RANDOM")
        .setImage(img)
        .setTitle(`From r/${random}`)
        .setURL(`https://reddit.com/r/${random}`);
        message.channel.send(embed)
    }
	}