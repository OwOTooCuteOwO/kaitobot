module.exports = {
    name: 'madlad',
    alias: ['mad'],
    description: 'Madlad',
	execute: async (message) => {
        const subReddits = ["Madlads"]
        const random = subReddits[Math.floor(Math.random() * subReddits.length)];
  
        const img = await randomPuppy(random);
        const embed = new Discord.MessageEmbed()
        .setColor("RANDOM")
        .setImage(img)
        .setTitle(`From r/${random}`)
        .setURL(`https://reddit.com/r/${random}`);
        message.channel.send(embed)
    }
    }